package com.project.converter;

import org.springframework.stereotype.Component;

import com.project.dto.TicketDto;
import com.project.model.Ticket;

@Component
public class TicketConverter implements ModelDtoConverter<Ticket, TicketDto> {

	@Override
	public TicketDto modelToDto(Ticket ticket) {
		TicketDto ticketDto = new TicketDto();
		ticketDto.setSubject(ticket.getSubject());
		ticketDto.setType(ticket.getType());
		ticketDto.setPriority(ticket.getPriority());
		ticketDto.setAssignee(ticket.getAssignee());
		ticketDto.setReporter(ticket.getReporter());
		ticketDto.setStatus(ticket.getStatus());
		ticketDto.setDescription(ticket.getDescription());
		ticketDto.setProject(ticket.getProject());
		ticketDto.setResolution(ticket.isResolution());
		return ticketDto;
	}

	@Override
	public Ticket dtoToModel(TicketDto ticketDto) {
		Ticket ticket = new Ticket();
		ticket.setSubject(ticketDto.getSubject());
		ticket.setType(ticketDto.getType());
		ticket.setPriority(ticketDto.getPriority());
		ticket.setDescription(ticketDto.getDescription());
		ticket.setProject(ticketDto.getProject());
		ticket.setReporter(ticketDto.getReporter());
		return ticket;
	}

}
