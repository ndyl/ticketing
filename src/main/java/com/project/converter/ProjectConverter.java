package com.project.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.project.dto.ProjectDto;
import com.project.model.Project;

@Component
public class ProjectConverter implements ModelDtoConverter<Project, ProjectDto> {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String DATE_TIME_FORMAT = DATE_FORMAT + " HH:mm";
	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
	
	@Override
	public ProjectDto modelToDto(Project project) {
		String startDateStr = dateTimeFormatter.format(project.getStartDate());
		Optional<Object> endDate = Optional.ofNullable(project.getEndDate());
		String endDateStr = (endDate.isPresent()) ? dateTimeFormatter.format(project.getEndDate()) : (String) endDate.orElse("");
		
		ProjectDto projectDto = new ProjectDto();
		projectDto.setName(project.getName());
		projectDto.setType(project.getType());
		projectDto.setProjectLead(project.getProjectLead());
		projectDto.setStartDate(startDateStr);
		projectDto.setEndDate(endDateStr);
		
		return projectDto;
	}

	@Override
	public Project dtoToModel(ProjectDto projectDto) {
		LocalTime nowTime = LocalTime.now();
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
		
		if (projectDto.getStartDate().isEmpty()) {
			projectDto.setStartDate(dateFormatter.format(LocalDate.now()));
		}

		LocalDate startDate = null;
		LocalDate endDate = null;
		try {
			startDate = LocalDate.parse(projectDto.getStartDate(), dateFormatter);
			if (!projectDto.getEndDate().isEmpty()) {
				endDate = LocalDate.parse(projectDto.getEndDate(), dateFormatter);
			}
		} catch (DateTimeParseException e) {
			e.printStackTrace();
		}

		String startDateTimeStr = LocalDateTime.of(startDate, nowTime).format(dateTimeFormatter);
		LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeStr, dateTimeFormatter);
		
		String endDateTimeStr = null;
		LocalDateTime endDateTime = null;
		if(endDate != null) {
			endDateTimeStr = LocalDateTime.of(endDate, nowTime).format(dateTimeFormatter);
			endDateTime = LocalDateTime.parse(endDateTimeStr, dateTimeFormatter);
		}
		
		Project project = new Project();
		project.setName(projectDto.getName());
		project.setType(projectDto.getType());
		project.setProjectLead(projectDto.getProjectLead());
		project.setStartDate(startDateTime);
		project.setEndDate(endDateTime);
		
		return project;
	}

}
