package com.project.converter;

public interface ModelDtoConverter<Model, Dto> {
	public Dto modelToDto(Model model);
	public Model dtoToModel(Dto dto);
}
