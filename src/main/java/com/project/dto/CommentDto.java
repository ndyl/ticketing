package com.project.dto;

import java.time.LocalDateTime;
//import java.util.Date;

import javax.validation.constraints.NotNull;

import com.project.model.Ticket;

public class CommentDto implements Dto {
	
	private String name;
	
	private Ticket ticket;
	
	private LocalDateTime createdDate;
	
	private String author;
	
	@NotNull(message="Comment has to have some text.")
	private String body;
	
	public CommentDto() {
		
	}
	
	public CommentDto(String name, LocalDateTime createdDate, String author, String body, Ticket ticket) {
		this.name = name;
		this.createdDate = createdDate;
		this.author = author;
		this.body = body;
		this.ticket = ticket;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
