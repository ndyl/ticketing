package com.project.dto;

import javax.validation.constraints.Size;

import com.project.model.Project;

public class TicketDto implements Dto {

	@Size(min=2, message="Subject must have at least {min} letters.")
	private String subject;

	private String type;

	private String priority;
	
	private String reportDate;

	private String assignee;

	private String reporter;

	private Project project;

	private String status;

	@Size(min=2, message="Description must have at least {min} letters.")
	private String description;

	private boolean resolution;
	
	public TicketDto() {
		
	}
	
	public TicketDto(String subject, String type, String priority, String assignee, String reporter, String status, String description, 
			String reportDate, Project project, boolean resolution) {
		this.subject = subject;
		this.type = type;
		this.priority = priority;
		this.assignee = assignee;
		this.reporter = reporter;
		this.status = status;
		this.description = description;
		this.reportDate = reportDate;
		this.project = project;
		this.resolution = resolution;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isResolution() {
		return resolution;
	}

	public void setResolution(boolean resolution) {
		this.resolution = resolution;
	}
}
