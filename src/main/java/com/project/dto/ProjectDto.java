package com.project.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class ProjectDto implements Dto {

	@Size(min=2, message="Name must have at least {min} letters.")
	private String name;

	private String type;

	@NotBlank(message="Project lead field cannot be blank.")
	private String projectLead;

	private String startDate;
	
	private String endDate;

	public ProjectDto() {
		
	}
	
	public ProjectDto(String name, String type, String projectLead, String startDate, String endDate) {
		this.name = name;
		this.type = type;
		this.projectLead = projectLead;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProjectLead() {
		return projectLead;
	}

	public void setProjectLead(String projectLead) {
		this.projectLead = projectLead;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
