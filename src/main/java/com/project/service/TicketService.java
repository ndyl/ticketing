package com.project.service;

import java.util.List;

import com.project.model.Ticket;

public interface TicketService {
	public void addTicket(Ticket ticket);
	public List<Ticket> listTickets();
	public List<Ticket> listActiveTickets();
	public List<Ticket> listTicketsOfProject(Integer projectId);
	public Ticket findById(Integer id);
	public void updateTicket(Ticket ticket);
	public List<Ticket> findByReporter(String reporter);
	public List<Ticket> findByAssignee(String assignee);
}
