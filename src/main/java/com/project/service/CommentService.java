package com.project.service;

import java.util.List;

import com.project.model.Comment;

public interface CommentService {
	public void addComment(Comment comment);
	public List<Comment> listComments();
	public Comment findById(Integer commentId);
	public void updateComment(Comment comment);
	public List<Comment> findByTicketId(Integer ticketId);
}
