package com.project.service;

//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import java.util.Date;
import java.util.List;
//import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.TicketDao;
import com.project.model.Ticket;

@Service
public class TicketServiceImpl implements TicketService{
	
	@Autowired
	TicketDao ticketDao;
	
	public void addTicket(Ticket ticket) {
		//setting default values for new ticket properties - status, resolution 
		//ticket status may be: New, Open, In Progess, Closed, Resolved
		ticket.setStatus("New");
		ticket.setResolution(false);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime date = LocalDateTime.now();
		String dateStr = date.format(formatter);
	    ticket.setReportDate(LocalDateTime.parse(dateStr, formatter));    
		
		ticketDao.addTicket(ticket);
	}
	
	public List<Ticket> listTickets() {
		List<Ticket> ticketsList = ticketDao.listTickets();
		return ticketsList;
	}
	
	public List<Ticket> listActiveTickets() {
		List<Ticket> ticketsList = ticketDao.listActiveTickets();
		return ticketsList;
	}
	
	public List<Ticket> listTicketsOfProject(Integer projectId) {
		List<Ticket> ticketsList = ticketDao.listTicketsOfProject(projectId);
		return ticketsList;
	}
	
	public Ticket findById(Integer id) {
		Ticket ticket = ticketDao.findById(id);
		return ticket;
	}
	
	public void updateTicket(Ticket ticket) {
		ticketDao.updateTicket(ticket);
	}
	
	public List<Ticket> findByReporter(String reporter){
		List<Ticket> ticketsList = ticketDao.findByReporter(reporter);
		return ticketsList;
	}
	
	public List<Ticket> findByAssignee(String assignee){
		List<Ticket> ticketsList = ticketDao.findByAssignee(assignee);
		return ticketsList;
	}
}
