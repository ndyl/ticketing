package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.ProjectDao;
import com.project.model.Project;

@Service
public class ProjectServiceImpl implements ProjectService{
	@Autowired
	ProjectDao projectDao;
	
	@Override
	public void addProject(Project project) {
		projectDao.addProject(project);
	}
	
	@Override
	public List<Project> listProjects(){
		return projectDao.listProjects();
	}
	
	@Override
	public List<Project> listActiveProjects(){
		return projectDao.listActiveProjects();
	}
	
	@Override
	public Project findById(Integer projectId){
		return projectDao.findById(projectId);
	}
	
	@Override
	public void updateProject(Project project){
		projectDao.updateProject(project);
	}
}
