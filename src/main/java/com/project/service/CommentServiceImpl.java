package com.project.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.CommentDao;
import com.project.model.Comment;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	CommentDao commentDao;
	
	@Override
	public void addComment(Comment comment) {
		LocalDateTime date = LocalDateTime.now();
	    comment.setCreatedDate(date); 
		commentDao.addComment(comment);
	}
	
	@Override
	public List<Comment> listComments(){
		return commentDao.listComments();
	}
	
	@Override
	public Comment findById(Integer commentId){
		return commentDao.findById(commentId);
	}
	
	@Override
	public void updateComment(Comment comment){
		commentDao.updateComment(comment);
	}
	
	@Override
	public List<Comment> findByTicketId(Integer ticketId){
		return commentDao.findByTicketId(ticketId);
	}
}

