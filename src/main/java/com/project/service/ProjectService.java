package com.project.service;

import java.util.List;

import com.project.model.Project;

public interface ProjectService {
	public void addProject(Project project);
	public List<Project> listProjects();
	public List<Project> listActiveProjects();
	public Project findById(Integer projectId);
	public void updateProject(Project project);
}
