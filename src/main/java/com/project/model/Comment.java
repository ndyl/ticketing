package com.project.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment implements Model {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="comment_id", unique = true)
	private Integer commentId;

	@Column(unique = true)
	private String name;

	@ManyToOne
	@JoinColumn(name="ticket_id", nullable=false)
	private Ticket ticket;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	private String author;

	private String body;

	public Comment(String name, LocalDateTime createdDate, String author, String body, Ticket ticket) {
		this.name = name;
		this.createdDate = createdDate;
		this.author = author;
		this.body = body;
		this.ticket = ticket;
	}

	public Comment() {
		
	}
	
	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
