package com.project.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Ticket implements Model {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ticket_id", unique = true)
	private Integer ticketId;
	
	private String subject;
	
	private String type;
	
	private String priority;
	
	@Column(name="report_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime reportDate;
	
	private String assignee;
	
	private String reporter;
	
	@ManyToOne
    @JoinColumn(name="project_id", nullable=false)
	private Project project;
	
	private String status;
	
	private String description;

	private Boolean resolution = false;
	
	@OneToMany(mappedBy = "ticket")
	private List<Comment> comments;
	
	public Ticket() {
		
	}
	
	public Ticket(String subject, String type, String priority, String assignee, String reporter, String status, String description, 
			LocalDateTime reportDate, Project project, Boolean resolution) {
		this.subject = subject;
		this.type = type;
		this.priority = priority;
		this.status = status;
		this.description = description;
		this.reportDate = reportDate;
		this.project = project;
		this.resolution = resolution;
	}
	
	public Ticket(String subject, String type, String priority, String description, Project project) {
		this.subject = subject;
		this.type = type;
		this.priority = priority;
		this.description = description;
		this.project = project;
	}

	public Ticket(String subject, String type, String priority, String assignee, String status, String description, Project project, Boolean resolution) {
		this.subject = subject;
		this.type = type;
		this.priority = priority;
		this.assignee = assignee;
		this.status = status;
		this.description = description;
		this.project = project;
		this.resolution = resolution;
	}
	
	public Ticket(String subject, String type, String priority, String assignee, String status, String description, boolean resolution) {
		this.subject = subject;
		this.type = type;
		this.priority = priority;
		this.assignee = assignee;
		this.status = status;
		this.description = description;
		this.resolution = resolution;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public LocalDateTime getReportDate() {
		return reportDate;
	}

	public void setReportDate(LocalDateTime reportDate) {
		this.reportDate = reportDate;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean isResolution() {
		return resolution;
	}

	public void setResolution(Boolean resolution) {
		this.resolution = resolution;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
}
