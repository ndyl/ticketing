package com.project.dao;

import java.util.List;

import com.project.model.Ticket;

public interface TicketDao {

	public void addTicket(Ticket ticket);
	public List<Ticket> listTickets();
	public List<Ticket> listActiveTickets();
	public List<Ticket> listTicketsOfProject(Integer projectId);
	public Ticket findById(Integer id);
	public List<Ticket> findByReporter(String reporter);
	public List<Ticket> findByAssignee(String assignee);
	public void updateTicket(Ticket ticket);	
}

