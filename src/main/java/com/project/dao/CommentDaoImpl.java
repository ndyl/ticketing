package com.project.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.model.Comment;

@Repository
public class CommentDaoImpl implements CommentDao {

	private List<Comment> commentsList = null;
	private Comment comment = null;
	private String queryStr;
	private SessionFactory sessionFactory;

	public CommentDaoImpl() {

	}

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	@Transactional
	public void addComment(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(comment);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Comment> listComments() {
		Session session = this.sessionFactory.getCurrentSession();

		queryStr = "SELECT c FROM Comment c";
		Query query = session.createQuery(queryStr);
		commentsList = query.getResultList();

		return commentsList;
	}

	@Override
	@Transactional
	public Comment findById(Integer commentId) {
		comment = null;

		Session session = this.sessionFactory.getCurrentSession();
		comment = new Comment();
		comment = session.find(Comment.class, commentId);

		return comment;
	}

	@Override
	@Transactional
	public void updateComment(Comment comment) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(comment);
	}
	
	@Override
	@Transactional
	public List<Comment> findByTicketId(Integer ticketId){
		Session session = this.sessionFactory.getCurrentSession();

		queryStr = "SELECT c FROM Comment c WHERE ticket_id=" + ticketId;
		Query query = session.createQuery(queryStr);
		commentsList = query.getResultList();

		return commentsList;		
	}
}
