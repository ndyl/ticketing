package com.project.dao;

import java.util.List;

import com.project.model.Project;

public interface ProjectDao {
	public void addProject(Project project);
	public List<Project> listProjects();
	public List<Project> listActiveProjects();
	public Project findById(Integer id);
	public void updateProject(Project project);
}
