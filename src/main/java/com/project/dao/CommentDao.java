package com.project.dao;

import java.util.List;

import com.project.model.Comment;

public interface CommentDao {
	
	public void addComment(Comment comment);
	public List<Comment> listComments();
	public Comment findById(Integer id);
	public void updateComment(Comment comment);
	public List<Comment> findByTicketId(Integer ticketId);
	
}
