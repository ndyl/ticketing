package com.project.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.model.Ticket;

@Repository
public class TicketDaoImpl implements TicketDao {

	private List<Ticket> ticketsList = null;
	private Ticket ticket = null;
	private String queryStr;
	private SessionFactory sessionFactory;

	public TicketDaoImpl() {

	}

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	@Transactional
	public void addTicket(Ticket ticket) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(ticket);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Ticket> listTickets() {
		Session session = this.sessionFactory.getCurrentSession();

		queryStr = "SELECT t FROM Ticket t";
		Query query = session.createQuery(queryStr);
		ticketsList = query.getResultList();

		return ticketsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Ticket> listActiveTickets() {
		Session session = this.sessionFactory.getCurrentSession();

		queryStr = "SELECT t FROM Ticket t WHERE t.status NOT IN ('Closed', 'Resolved')";
		Query query = session.createQuery(queryStr);
		ticketsList = query.getResultList();

		return ticketsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Ticket> listTicketsOfProject(Integer projectId) {
		Session session = this.sessionFactory.getCurrentSession();
		queryStr = "SELECT t FROM Ticket t INNER JOIN t.project p WHERE p.projectId = :id";
		Query query = session.createQuery(queryStr).setParameter("id", projectId);
		List<Ticket> ticketsList = query.getResultList();

		return ticketsList;
	}

	@Override
	@Transactional
	public Ticket findById(Integer ticketId) {
		ticket = null;

		Session session = this.sessionFactory.getCurrentSession();
		ticket = new Ticket();
		ticket = session.find(Ticket.class, ticketId);

		return ticket;
	}

	@Override
	@Transactional
	public void updateTicket(Ticket ticket) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(ticket);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Ticket> findByReporter(String reporter) {
		Session session = this.sessionFactory.getCurrentSession();
		
		queryStr = "SELECT t FROM Ticket t WHERE t.reporter = :reporter";
		Query query = session.createQuery(queryStr).setParameter("reporter", reporter);
		ticketsList = query.getResultList();

		return ticketsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Ticket> findByAssignee(String assignee) {
		Session session = this.sessionFactory.getCurrentSession();

		queryStr = "SELECT t FROM Ticket t WHERE t.assignee = :assignee";
		Query query = session.createQuery(queryStr).setParameter("assignee", assignee);
		ticketsList = query.getResultList();

		return ticketsList;
	}
}
