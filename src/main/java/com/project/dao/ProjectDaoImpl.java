package com.project.dao;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.model.Project;

@Repository
public class ProjectDaoImpl implements ProjectDao {

	private List<Project> projectsList = null;
	private Project project = null;
	private String queryStr;
	private SessionFactory sessionFactory;

	public ProjectDaoImpl() {

	}

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	@Transactional
	public void addProject(Project project) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(project);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Project> listProjects() {
		Session session = this.sessionFactory.getCurrentSession();
		queryStr = "SELECT p FROM Project p";
		Query query = session.createQuery(queryStr);
		projectsList = query.getResultList();

		return projectsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Project> listActiveProjects() {
		Session session = this.sessionFactory.getCurrentSession();
		queryStr = "SELECT p FROM Project p WHERE p.endDate IS NULL OR p.endDate > CURDATE()";
		Query query = session.createQuery(queryStr);
		projectsList = query.getResultList();

		return projectsList;
	}
	
	@Override
	@Transactional
	public Project findById(Integer projectId) {
		project = null;

		Session session = this.sessionFactory.getCurrentSession();
		project = new Project();
		project = session.find(Project.class, projectId);

		return project;
	}

	@Override
	@Transactional
	public void updateProject(Project project) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(project);
	}

}
