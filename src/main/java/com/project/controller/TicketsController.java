package com.project.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.converter.TicketConverter;
import com.project.dto.CommentDto;
import com.project.dto.TicketDto;
import com.project.model.Comment;
import com.project.model.Project;
import com.project.model.Ticket;
import com.project.service.CommentService;
import com.project.service.ProjectService;
import com.project.service.TicketService;

@Controller
@RequestMapping("/tickets")
public class TicketsController {
	private static final String NEW_TICKET_URI = "/project={projectId}/newTicket";
	private static final String LIST_TICKETS_URI = "/list";
	private static final String TICKET_ID = "/ticket={ticketId}";
	private static final String EDIT_TICKET_URI = TICKET_ID + "/edit";
	private static final String UPDATE_TICKET_URI = TICKET_ID + "/update";
	private static final String TICKET_DETAILS_URI = TICKET_ID + "/details";
	private static final String ADD_COMMENT_TO_TICKET_URI = TICKET_ID + "/newComment";
	private static final String LIST_TICKET_COMMENTS_URI = TICKET_ID + "/listComments";
	private static final String REPORTED_TICKETS_URI = "/reported";
	private static final String ASSIGNED_TICKETS_URI = "/assigned";
	
	@Autowired
	private TicketService ticketService;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private TicketConverter ticketConverter;
	
	@RequestMapping(value = NEW_TICKET_URI, method = RequestMethod.GET)
	public String addTicket(@PathVariable("projectId") Integer projectId, Model model){
        model.addAttribute("ticket", new TicketDto());
        model.addAttribute("projectId", projectId);
        
	    
        Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		 return "newTicketPage";
	}
	
	@RequestMapping(value = NEW_TICKET_URI, method = RequestMethod.POST)
	public String addTicket(@ModelAttribute("ticket") TicketDto ticketDto, @PathVariable("projectId") Integer projectId, BindingResult result, ModelMap model){
		if (result.hasErrors()) {		
			return "newTicketPage";
		}
		//get reporter name from session and set it to Ticket object
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		String name = authentication.getName();
		ticketDto.setReporter(name);
		
		Project project = projectService.findById(projectId);
		ticketDto.setProject(project);
		
		Ticket ticket = ticketConverter.dtoToModel(ticketDto);
		ticketService.addTicket(ticket);
		model.addAttribute("ticket", ticket);
		
		//add role in order to display proper view (go to admin panel or go to user panel button)
	    Map<String, Boolean> roles = getRoles(authentication);	
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		
		return "successTicketAdd";
	}
	
	@RequestMapping(value = LIST_TICKETS_URI, method=RequestMethod.GET)
	public String listTickets(Model model) {
	    List<Ticket> ticketsList = ticketService.listTickets();
	    model.addAttribute("ticketsList", ticketsList);
	    
	    Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
	    
	    return "ticketsPage";
	}
	
    @RequestMapping(path = EDIT_TICKET_URI, method = RequestMethod.GET)
    public String editTicket(Model model, @PathVariable(value = "ticketId") Integer ticketId) {
        Ticket ticket = ticketService.findById(ticketId);
        TicketDto ticketDto = ticketConverter.modelToDto(ticket);
        model.addAttribute("ticket", ticketDto);
    	model.addAttribute("ticketId", ticketId);
        
        return "editTicket";
    }
    
    @RequestMapping(path = UPDATE_TICKET_URI, method = RequestMethod.POST)
    public String updateTicket(@Valid @ModelAttribute("ticket") TicketDto ticketDto, BindingResult result, @PathVariable("ticketId") Integer ticketId) {
    	
    	if (result.hasErrors()) {
			return "editTicket";
		}
    	
    	if (ticketDto.isResolution() == true) {
    		ticketDto.setStatus("Resolved");
    	}
    	
    	//Ticket ticket = ticketService.findById(ticketId);
    	Ticket ticket = ticketConverter.dtoToModel(ticketDto);
    	ticket.setTicketId(ticketId);
    	/*ticket.setSubject(ticketDto.getSubject());
    	ticket.setType(ticketDto.getType());
    	ticket.setPriority(ticketDto.getPriority());
    	ticket.setAssignee(ticketDto.getAssignee());
    	ticket.setStatus(ticketDto.getStatus());
    	ticket.setDescription(ticketDto.getDescription());
    	ticket.setResolution(ticketDto.isResolution());
    	*/
    	
    	ticketService.updateTicket(ticket);
    	return "redirect:/tickets" + LIST_TICKETS_URI;
    }
	
    @RequestMapping(path = TICKET_DETAILS_URI, method = RequestMethod.GET)
    public String ticketDetails(Model model, @PathVariable(value = "ticketId") Integer ticketId) {
        Ticket ticket = ticketService.findById(ticketId);
    	model.addAttribute("ticket", ticket);
    	
		//add role in order to display proper view (go to admin panel or go to user panel button)
		//get reporter name from session and set it to Ticket object
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		//String name = authentication.getName();
    	
    	Map<String, Boolean> roles = getRoles(authentication);	
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
    	
        return "ticketDetails";
    }
  
	@RequestMapping(value = ADD_COMMENT_TO_TICKET_URI, method = RequestMethod.GET)
	public String addComment(Model model, @PathVariable(value = "ticketId") Integer ticketId){
        CommentDto commentDto = new CommentDto();
        Ticket ticket = ticketService.findById(ticketId);
        
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		
        commentDto.setTicket(ticket);
		model.addAttribute("comment", commentDto);
		model.addAttribute("ticketId", ticketId);

        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		 return "newCommentPage :: commentForm";
	}

	@RequestMapping(value = ADD_COMMENT_TO_TICKET_URI, method = RequestMethod.POST)
	public String addComment(@ModelAttribute("comment") CommentDto commentDto,  @PathVariable(value = "ticketId") Integer ticketId, BindingResult result, ModelMap model){
		if (result.hasErrors()) {		
			return "ticketDetails";
		}
		//get reporter name from session and set it to Ticket object
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		String name = authentication.getName();
		
		Ticket ticket = ticketService.findById(ticketId);
		Comment newComment = new Comment();
		newComment.setAuthor(name);
		newComment.setBody(commentDto.getBody());
		newComment.setTicket(ticket);
		
		commentService.addComment(newComment);
		model.addAttribute("comment", newComment);
		
		//add role in order to display proper view (go to admin panel or go to user panel button)
	   
		Map<String, Boolean> roles = getRoles(authentication);	
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		return "successCommentAdd";
	}	
	
    
	//Method returns roles of session user
	@SuppressWarnings("unchecked")
	private static HashMap<String, Boolean> getRoles (Authentication authentication){
	    Collection<GrantedAuthority> credentials = (Collection<GrantedAuthority>) authentication.getAuthorities();
	    HashMap<String, Boolean> roles = new HashMap<>();

	    for (GrantedAuthority authority : credentials) {
	           roles.put(authority.getAuthority(), true);
	    }
	    return roles;
	}
	
	@RequestMapping(value = LIST_TICKET_COMMENTS_URI, method = RequestMethod.GET)
	public String listTicketComments(Model model,  @PathVariable(value = "ticketId") Integer ticketId){
		List<Comment> commentsList = commentService.findByTicketId(ticketId);
	    model.addAttribute("commentsList", commentsList);
		
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		 return "commentsPage :: commentsList";
	}
	
	
	@RequestMapping(value = REPORTED_TICKETS_URI, method=RequestMethod.GET)
	public String getReportedTickets(Model model) {
		//get name of reporter
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		String reporterName = authentication.getName();
			
		//get user role
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		
	    List<Ticket> ticketsList = ticketService.findByReporter(reporterName);
	    model.addAttribute("ticketsList", ticketsList);
	    return "ticketsPage";
	} 
	@RequestMapping(value = ASSIGNED_TICKETS_URI, method=RequestMethod.GET)
	public String getAssignedTickets(Model model) {
		//get name of reporter
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		String assigneeName = authentication.getName();
		
		//get user role
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
	    List<Ticket> ticketsList = ticketService.findByAssignee(assigneeName);
	    model.addAttribute("ticketsList", ticketsList);
	    return "ticketsPage";
	}
}
