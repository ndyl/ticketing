package com.project.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.converter.ProjectConverter;
import com.project.dto.ProjectDto;
import com.project.model.Project;
import com.project.service.ProjectService;

@Controller
@RequestMapping("/projects")
public class ProjectsController {
	private static final String PROJECT_ID = "/project={projectId}";
	private static final String NEW_PROJECT_URI = "/new";
	private static final String LIST_PROJECTS_URI = "/list";
	private static final String ACTIVE_PROJECTS_URI = "/active";
	private static final String EDIT_PROJECT = PROJECT_ID + "/edit";
	private static final String UPDATE_PROJECT_URI = PROJECT_ID + "/update";

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProjectConverter projectConverter;

	@RequestMapping(value = NEW_PROJECT_URI, method = RequestMethod.GET)
	public String addProject(Model model){
		model.addAttribute("project", new ProjectDto());
		return "newProjectPage";
	}

	@RequestMapping(value = NEW_PROJECT_URI, method = RequestMethod.POST)
	public String addProject(@Valid @ModelAttribute("project") ProjectDto projectDto, BindingResult result, ModelMap model){
		/*
		 * TODO: Validation
		if (endDate != null && startDate.isAfter(endDate)) {
			result.rejectValue("endDate", "error.project", "End date cannot be before start date.");
		}
		 */

		if (result.hasErrors()) {	
			System.out.println("Errors in newProject");
			return "newProjectPage";
		}	

		Project project = projectConverter.dtoToModel(projectDto);
		projectService.addProject(project);
		model.addAttribute("project", project);

		Authentication authentication = SecurityContextHolder
				.getContext()
				.getAuthentication();
		Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}

		return "success";
	} 

	@RequestMapping(value = LIST_PROJECTS_URI, method=RequestMethod.GET)
	public String listProjects(Model model) {
		List<Project> projectsList = projectService.listProjects();
		model.addAttribute("projectsList", projectsList);

		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}

		return "projectsPage";
	}

	@RequestMapping(value = ACTIVE_PROJECTS_URI, method=RequestMethod.GET)
	public String listActiveProjects(Model model) {
		List<Project> projectsList = projectService.listActiveProjects();
		model.addAttribute("projectsList", projectsList);

		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}

		return "activeProjectsPage";
	}

	@RequestMapping(path = EDIT_PROJECT, method = RequestMethod.GET)
	public String editProject(Model model, @PathVariable(value = "projectId") Integer projectId) {
		Project project = projectService.findById(projectId);
		ProjectDto projectDto = projectConverter.modelToDto(project);
		model.addAttribute("project", projectDto);
		model.addAttribute("projectId", projectId);
		return "editProject";
	}  

	@RequestMapping(path = UPDATE_PROJECT_URI, method = RequestMethod.POST)
	public String updateProject(@Valid @ModelAttribute("project")ProjectDto projectDto, @PathVariable("projectId") Integer projectId, BindingResult result) {
		/* TODO
		if (endDate != null && startDate.isAfter(endDate)) {
			result.addError(new ObjectError("endDate", "End date cannot be before start date."));
			//result.rejectValue("endDate", "error.project", "End date cannot be before start date.");
		}
		 */
		if (result.hasErrors()) {		
			return "editProject";
		}

		Project project = projectConverter.dtoToModel(projectDto);
		project.setProjectId(projectId);
		projectService.updateProject(project);

		return "redirect:/projects" + LIST_PROJECTS_URI;
	}

	//Method returns roles of session user
	private static HashMap<String, Boolean> getRoles (Authentication authentication){
		@SuppressWarnings("unchecked")
		Collection<GrantedAuthority> credentials = (Collection<GrantedAuthority>) authentication.getAuthorities();
		HashMap<String, Boolean> roles = new HashMap<>();

		for (GrantedAuthority authority : credentials) {
			roles.put(authority.getAuthority(), true);
		}
		return roles;
	}
}
