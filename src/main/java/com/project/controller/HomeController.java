package com.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "index";
    }
    
    @RequestMapping(value = "/userPanel", method = RequestMethod.GET)
    public String userPanel() {
        return "userPanel";
    }
     
    @RequestMapping(value = "/adminPanel", method = RequestMethod.GET)
    public String adminPage() {
        return "adminPanel";
    }
    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String accessDenied(){
    	return "accessDenied";
    }
}